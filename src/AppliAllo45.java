import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class AppliAllo45 extends Application {

    private Button bConnexion;
    private Button bDeconnexion;
    private Scene scene;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(AppliAllo45.class, args);
    }

    @Override
    public void init() {
        this.bConnexion = new Button("Connexion");
        ControleurBouton control = new ControleurBouton(this);
        this.bConnexion.setOnAction(control);
        this.bDeconnexion = new Button("Déconnexion");
    }

    @Override
    public void start(Stage stage) {
        Pane root = new fenetreConnexion(this.bConnexion);
        this.scene = new Scene(root, 1920, 1080);
        // root.setStyle("-fx-background-image: url(fond.jpg)");
        root.setStyle("-fx-background-color: #303444");
        stage.setScene(this.scene);
        stage.setTitle("Application Allo 45");
        stage.show();
    }

    public void afficheFenetreConnexion() {
        Pane root = new fenetreConnexion(this.bConnexion);
        root.setStyle("-fx-background-color: #303444");
        this.scene.setRoot(root);
    }

    public void afficherFenetreAccueil() {
        Pane root = new FenetreAccueil(this.bDeconnexion, this);
        // mettre une image en fond d'écran dans le pane
        root.setStyle("-fx-background-color: #303444");
        this.scene.setRoot(root);
    }

    public void afficherFenetreQuestionnaire() {
        Pane root = new FenetreQuestionnaire(this);
        root.setStyle("-fx-background-color: #303444");
        this.scene.setRoot(root);
    }

    public void afficherFenetreSondeur() {
        Pane root = new FenetreSondeur(this);
        root.setStyle("-fx-background-color: #303444");
        this.scene.setRoot(root);
    }

    public Alert popUpLabelInexistant(){
        // A implementer
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setContentText("Ce label n'existe pas");
        return alert;
    }

    public void afficherFenetreCreationQuestionnaire() {
        Pane root = new FenetreCreationQuestionnaire(this);
        root.setStyle("-fx-background-color: #303444");
        this.scene.setRoot(root);
    }
}
