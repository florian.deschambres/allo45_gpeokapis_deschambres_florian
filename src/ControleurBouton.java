
import javafx.event.EventHandler;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class ControleurBouton implements EventHandler<ActionEvent> {

    private AppliAllo45 appli;

    public ControleurBouton(AppliAllo45 appli) {
        this.appli = appli;
    }

    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) (event.getSource());
        if (button.getText().equals("Connexion")) {
            this.appli.afficherFenetreAccueil();

        } else if (button.getText().equals("Panel Questionnaire")) {
            this.appli.afficherFenetreQuestionnaire();

        } else if (button.getText().equals("Panel sondeur")) {
            this.appli.afficherFenetreSondeur();

        } else if (button.getText().equals("Panel Stat")) {
            this.appli.popUpLabelInexistant().showAndWait();

        } else if (button.getText().equals("Déconnexion")) {
            this.appli.afficheFenetreConnexion();

        } else if (button.getText().equals("Accueil")) {
            this.appli.afficherFenetreAccueil();

        } else if (button.getText().equals("Questions")) {
            this.appli.afficherFenetreQuestionnaire();

        } else if (button.getText().equals("Sondeur")) {
            this.appli.afficherFenetreSondeur();

        } else if (button.getText().equals("quitter")) {
            this.appli.afficherFenetreAccueil();

        } else if (button.getText().equals("Créer un nouveau questionnaire !")){
            this.appli.afficherFenetreCreationQuestionnaire();

        } else if (button.getText().equals("Retour création/séléction")){
            this.appli.afficherFenetreQuestionnaire();
        }
    }
}