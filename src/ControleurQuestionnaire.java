import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.event.ActionEvent;

public class ControleurQuestionnaire implements EventHandler<ActionEvent> {

    private FenetreCreationQuestionnaire ques;

    public ControleurQuestionnaire(FenetreCreationQuestionnaire ques) {
        this.ques = ques;
    }

    @Override
    public void handle(ActionEvent event) {
        Button button = (Button) (event.getSource());
        if (button.getText().equals("Ajouter une question")){
            System.out.println("marche");
            this.ques.questionConcepteur();
        }else if (button.getText().equals("✓")){
            this.ques.creeLabel();
        }
    }
}
    

