import java.awt.Color;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class FenetreAccueil extends BorderPane {
    private Button boutonDeco;
    private AppliAllo45 app;
    private Button boutonPanelQuestionnaire;
    private Button boutonPanelSondeur;
    private Button boutonPanelStat;
    private Label erreur;
    // private MenuBar menuBar;

    public FenetreAccueil(Button deco, AppliAllo45 app) {
        // this.menuBar = new MenuBar();
        this.boutonDeco = deco;
        ControleurBouton control = new ControleurBouton(this.app);
        this.boutonDeco.setOnAction(control);
        this.app = app;
        this.boutonPanelQuestionnaire = new Button("Panel Questionnaire");
        ControleurBouton control2 = new ControleurBouton(this.app);
        this.boutonPanelQuestionnaire.setOnAction(control2);
        this.boutonPanelSondeur = new Button("Panel sondeur");
        ControleurBouton control3 = new ControleurBouton(this.app);
        this.boutonPanelSondeur.setOnAction(control3);
        this.boutonPanelStat = new Button("Panel Stat");
        ControleurBouton control4 = new ControleurBouton(this.app);
        this.boutonPanelStat.setOnAction(control4);
        this.setTop(this.hb());
        this.setCenter(this.bouton());
        this.setBottom(this.image());
        this.setLeft(this.image2());
        
    }

    private HBox hb() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.TOP_LEFT);

        Label lab = new Label("Accueil");
        lab.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab.setPadding(new Insets(10, 10, 10, 10));
        lab.setStyle("-fx-background-color: red;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        Label lab2 = new Label("Sondeur");
        lab2.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab2.setPadding(new Insets(10, 10, 10, 10));
        lab2.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        Label lab3 = new Label("Questionnaire");
        lab3.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab3.setPadding(new Insets(10, 10, 10, 10));
        lab3.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        Label lab4 = new Label("Statistiques");
        lab4.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab4.setPadding(new Insets(10, 10, 10, 10));
        lab4.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        Label lab5=new Label("Connecté en tant que : GUEYE BA Moussa") ;
        lab5.setFont(Font.font("Arial", FontWeight.BOLD, 15));
        lab5.setPadding(new Insets(15, 10, 10, 10));
        lab5.setStyle("-fx-text-fill: white;");
    

        hb.getChildren().addAll(lab, lab2, lab3, lab4,lab5);
        return hb;
    }

    private ImageView image(){
        ImageView image = new ImageView(new Image("iuto.png",70,60,true,true));
        //mettre image en bas à gauche
        image.setTranslateX(1200);
        return image;
    }

    private ImageView image2(){
        ImageView image2 = new ImageView(new Image("Photo.jpg",60,50,true,true));
        //mettre image en bas à gauche
        image2.setTranslateX(950);
        image2.setTranslateY(-50);
        image2.setStyle("-fx-border-radius:25;");
        return image2;
    }


    // private Label lab() {
    // Label label = new Label("Accueil :");
    // return label;
    // }

    public void visible(Label l){
        l.setVisible(true);
    }

    private VBox bouton() {
        VBox bouton = new VBox();
        //espace entre les boutons
       
        //garder même taille des boutons

        bouton.getChildren().addAll(this.boutonPanelQuestionnaire, this.boutonPanelSondeur, this.boutonPanelStat,this.boutonDeco);
        this.boutonPanelSondeur.setOnAction(new ControleurBouton(this.app));
        this.boutonPanelStat.setOnAction(new ControleurBouton(this.app));
        this.boutonDeco.setOnAction(new ControleurBouton(this.app));

        bouton.setAlignment(Pos.CENTER);
        bouton.setSpacing(20);
        bouton.setStyle("-fx-background-color: #6B7AB9;-fx-border-color: black;-fx-border-radius: 15;-fx-background-radius: 15;");

        this.boutonPanelQuestionnaire.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 15;-fx-background-radius: 15;");
        this.boutonPanelSondeur.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 15;-fx-background-radius: 15;");
        this.boutonPanelStat.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 15;-fx-background-radius: 15;");
        this.boutonDeco.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 15;-fx-background-radius: 15;");

        this.boutonPanelQuestionnaire.setMaxSize(200, 200);
        this.boutonPanelSondeur.setMaxSize(200, 200);
        this.boutonPanelStat.setMaxSize(200, 200);
        this.boutonDeco.setMaxSize(200, 200);

        //augmenter taille boutons
        this.boutonPanelQuestionnaire.setPrefSize(200, 40);
        this.boutonPanelSondeur.setPrefSize(200, 40);
        this.boutonPanelStat.setPrefSize(200, 40);
        this.boutonDeco.setPrefSize(200, 40);
        return bouton;
    }
}