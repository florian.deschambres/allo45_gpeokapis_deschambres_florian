import java.util.ArrayList;
import java.util.List;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class FenetreCreationQuestionnaire extends BorderPane{
    private AppliAllo45 appli;
    private Questionnaire questionnaire;
    private Button boutonCreerQuestion;
    private Label question;
    private VBox vbo;
    private String save;
    private TextField laquestion;
    private TextField lareponse;
    private Button validation;
    private Button quitterQuestionnaire;
    private List<Question> list;
    private int cpt;
    private TextField titreQ;

    
    public FenetreCreationQuestionnaire(AppliAllo45 appli){
        this.cpt = 0;
        this.appli = appli;
        this.quitterQuestionnaire = new Button("Retour création/séléction");
        this.boutonCreerQuestion = new Button("Ajouter une question");
        boutonCreerQuestion.setOnAction(new ControleurQuestionnaire(this));
        ControleurBouton controleur = new ControleurBouton(this.appli);
        this.quitterQuestionnaire.setOnAction(controleur);
        this.question = new Label();
        this.setTop(this.hb());
        this.setCenter(this.scroling());
        this.setBottom(this.quitterQuestionnaire);
    }

    private ScrollPane scroling() {
        ScrollPane scrolling = new ScrollPane();
        scrolling.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        scrolling.setContent(this.vbox());
        return scrolling;
    }


    public VBox vbox(){
        this.vbo = new VBox();
        Label titrePage = new Label("Création de questionnaire :");
        titrePage.setFont(Font.font("white", FontWeight.BOLD, 28));
        Label titreQuest = new Label("Titre du questionnaire :");
        titreQuest.setFont(Font.font("white", FontWeight.BOLD, 20));
        this.titreQ = new TextField();
        this.titreQ.setMaxWidth(280);
        this.validation = new Button("✓");
        this.validation.setOnAction(new ControleurQuestionnaire(this));
        this.vbo.setAlignment(Pos.CENTER);
        this.list = new ArrayList<>();
        this.questionnaire = new Questionnaire("", list);
        titrePage.setPadding(new Insets(50, 30, 30, 30));
        this.vbo.setSpacing(10);
        this.vbo.getChildren().addAll(titrePage, boutonCreerQuestion, titreQuest, this.titreQ, this.validation);
        vbo.setAlignment(Pos.CENTER);
        return this.vbo;
    }


    public TextField questionConcepteur(){
        this.laquestion = new TextField();
        this.laquestion.setPromptText("Entrez votre question");
        this.laquestion.setMaxWidth(280);
        this.laquestion.setAlignment(Pos.CENTER);
        this.validation = new Button("✓");
        this.validation.setOnAction(new ControleurQuestionnaire(this)); 
        this.vbo.getChildren().addAll(this.laquestion,this.validation);
        return this.laquestion;
    }


    public Label creeLabel(){
        if (cpt == 0){
            this.question = new Label();
            this.question.setText(this.titreQ.getText());
            this.question.setFont(Font.font("white", FontWeight.BOLD, 17));
            this.vbo.getChildren().addAll(this.question);
            this.vbo.getChildren().remove(this.titreQ);
            this.vbo.getChildren().remove(this.validation);
            Label passageQ = new Label("Les questions :");
            passageQ.setFont(Font.font("white", FontWeight.BOLD, 20));
            this.vbo.getChildren().addAll(passageQ);
            this.cpt +=1;
        }else{
            this.question = new Label();
            this.question.setText(this.laquestion.getText());
            this.question.setFont(Font.font("white", FontWeight.BOLD, 12));
            Question newQue = new Question(this.laquestion.getText());
            this.list.add(newQue);
            this.vbo.getChildren().addAll(this.question);
            this.vbo.getChildren().remove(this.laquestion);
            this.vbo.getChildren().remove(this.validation);
        }
        return this.question;
    }


    public Label afficheLabelQuestion(TextField t){
        String a = t.getText();
        Label question = new Label(a);
        return question;
    }

    private HBox hb() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.TOP_LEFT);

        Label lab = new Label("Accueil");
        lab.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab.setPadding(new Insets(10, 10, 10, 10));
        lab.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        Label lab2 = new Label("Sondeur");
        lab2.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab2.setPadding(new Insets(10, 10, 10, 10));
        lab2.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        Label lab3 = new Label("Questionnaire");
        lab3.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab3.setPadding(new Insets(10, 10, 10, 10));
        lab3.setStyle("-fx-background-color: red;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        Label lab4 = new Label("Statistiques");
        lab4.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab4.setPadding(new Insets(10, 10, 10, 10));
        lab4.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        hb.getChildren().addAll(lab,lab2,lab3,lab4);

        return hb;
    }

}
