import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.control.Label;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class FenetreQuestionnaire extends BorderPane {

    private Button creerQuestionnaire;
    private Button selectQuestionnaire;
    private AppliAllo45 appli;
    private Button quitter;

    public FenetreQuestionnaire(AppliAllo45 appli) {
        super();
        this.appli = appli;
        this.quitter = new Button("quitter");
        this.creerQuestionnaire = new Button("Créer un nouveau questionnaire !");
        this.selectQuestionnaire = new Button("Sélectionner un questionnaire !");
        this.creerQuestionnaire.setMaxSize(300, 300);
        this.selectQuestionnaire.setMaxSize(300, 300);
        BorderPane.setAlignment(this.quitter, Pos.CENTER_RIGHT);
        this.setTop(this.hb());
        this.setCenter(this.lavb());
        this.setBottom(this.quitter);
        ControleurBouton controleur = new ControleurBouton(this.appli);
        this.quitter.setOnAction(controleur);
        this.creerQuestionnaire.setOnAction(controleur);
        this.selectQuestionnaire.setOnAction(controleur);
    }

    private HBox hb() {
        HBox hb = new HBox();
        hb.setAlignment(Pos.TOP_LEFT);

        Label lab = new Label("Accueil");
        lab.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab.setPadding(new Insets(10, 10, 10, 10));
        lab.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        Label lab2 = new Label("Sondeur");
        lab2.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab2.setPadding(new Insets(10, 10, 10, 10));
        lab2.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        Label lab3 = new Label("Questionnaire");
        lab3.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab3.setPadding(new Insets(10, 10, 10, 10));
        lab3.setStyle("-fx-background-color: red;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        Label lab4 = new Label("Statistiques");
        lab4.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        lab4.setPadding(new Insets(10, 10, 10, 10));
        lab4.setStyle("-fx-background-color: white;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");

        hb.getChildren().addAll(lab,lab2,lab3,lab4);

        return hb;
    }

    private VBox lavb() {
        VBox lavb = new VBox();
        lavb.getChildren().addAll(this.creerQuestionnaire, this.selectQuestionnaire);
        lavb.setSpacing(20);
        lavb.setAlignment(Pos.CENTER);
        lavb.setStyle("-fx-background-color: #6B7AB9;-fx-border-color: black;-fx-border-radius: 5;-fx-background-radius: 5;");
        return lavb;
    }
}