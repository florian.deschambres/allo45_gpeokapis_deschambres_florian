public class Question {
    private String titre;

    public Question(String titre){
        this.titre = titre;
    }

    public String getTitre(){
        return this.titre;
    }

     public void creerQuestion(String titre){
        Question question = new Question(titre);    
    }
   
}