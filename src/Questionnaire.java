import java.util.List;

public class Questionnaire {
    private String titre;
    private List<Question> listeQ;

    public Questionnaire(String titre, List<Question> listeQ) {
        this.titre = titre;
        this.listeQ = listeQ;
    }

    public String getTitre() {
        return this.titre;
    }

    public List<Question> getListe(){
        return this.listeQ;
    }

    public void setTitre(String newTitre){
        this.titre = newTitre;
    }

    public void creerQuestionnaire(String titre, List<Question> listeQ){
        Questionnaire questio = new Questionnaire(titre, listeQ);
    }
}