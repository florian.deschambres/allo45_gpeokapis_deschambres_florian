import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class fenetreConnexion extends BorderPane {

    private TextField username;

    private PasswordField password;

    private Button bConnexion;

    public fenetreConnexion(Button bConnexion) {
        super();
        this.bConnexion = bConnexion;
        this.bConnexion.setStyle("-fx-border-color: LIGHTGRAY;-fx-border-radius: 5;-fx-background-radius: 5;-fx-background-color: #303444;");
        this.bConnexion.setTextFill(Color.WHITE);
        this.bConnexion.setMinWidth(150);
        this.setLeft(this.gauche());
        this.setRight(this.droite());
        //this.setAlignment(this.droite(), Pos.RIGHT);
    }

    private GridPane gauche(){
        GridPane gauche = new GridPane();

        Label l = new Label("Bienvenue sur Allo 45");
        l.setFont(Font.font("white", FontWeight.BOLD, 28));
        l.setTextFill(Color.LIGHTGRAY);

        l.setAlignment(Pos.CENTER);
        gauche.add(l,0,1);
        GridPane.setHalignment(l, HPos.CENTER);
        gauche.setAlignment(Pos.CENTER);
        gauche.setVgap(10);
        gauche.setStyle("-fx-background-color: #303444;");
        gauche.setPadding(new Insets(20,140,20,140));
        return gauche;
    }

    private GridPane droite(){

        GridPane droite = new GridPane();
        Label l = new Label("Bienvenue");
        l.setFont(Font.font("white", FontWeight.BOLD, 28));
        l.setTextFill(Color.web("#303444"));

        ImageView imv = new ImageView();
        Image image_login = new Image("login.png", 50, 50, false, false);
        imv.setImage(image_login);

        HBox image_username = new HBox();
        ImageView imv2 = new ImageView();
        Image image_login2 = new Image("login.png", 20, 20, false, false);
        this.username = new TextField();
        this.username.setMinHeight(30);
        this.username.setMinWidth(250);
        this.username.setStyle("-fx-border-color: #303444;-fx-border-radius: 5;-fx-background-radius: 5;-fx-background-color:rgba(128, 128, 128, 0.2);");
        this.username.setPromptText("Nom d'utilisateur");
        imv2.setImage(image_login2);
        image_username.getChildren().addAll(imv2, this.username);
        image_username.setSpacing(10);
        image_username.setAlignment(Pos.CENTER);

        HBox image_password = new HBox();
        ImageView imv3 = new ImageView();
        Image image_cadna = new Image("cadna.png", 20, 20, false, false);
        this.password = new PasswordField();
        this.password.setMinHeight(30);
        this.password.setMinWidth(250);
        this.password.setStyle("-fx-border-color: #303444;-fx-border-radius: 5;-fx-background-radius: 5;-fx-background-color:rgba(128, 128, 128, 0.2);");
        this.password.setPromptText("Mot de passe");
        imv3.setImage(image_cadna);
        image_password.getChildren().addAll(imv3, this.password);
        image_password.setSpacing(10);
        image_password.setAlignment(Pos.CENTER);

        droite.add(l, 0, 0);
        droite.add(imv, 0, 1);
        droite.add(image_username, 0, 2);
        droite.add(image_password, 0, 3);
        droite.add(bConnexion, 0, 4);
        GridPane.setHalignment(l, HPos.CENTER);
        GridPane.setHalignment(this.bConnexion, HPos.CENTER);
        GridPane.setHalignment(imv, HPos.CENTER);
        droite.setAlignment(Pos.CENTER);
        droite.setVgap(10);
        droite.setStyle("-fx-background-color: WHITE;");
        droite.setPadding(new Insets(20,200,20,200));
        return droite;
    }
}
